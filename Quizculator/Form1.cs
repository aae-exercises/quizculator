﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quizculator
{
    public partial class Form1 : Form
    {
        Quiz quiz;
        int time;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            NewGame();
        }

        private void NewGame() {
            foreach (NumericUpDown n in this.Controls.OfType<NumericUpDown>()) n.Value = 0;
            label6.Text = "2:00";
            label6.ForeColor = Color.Green;
            time = 120;
            quiz = new Quiz();
            UpdateQuestions();
            timer1.Start();
        }

        private void UpdateQuestions() {
            label1.Text = quiz.getQuestions()[0].ToString();
            label2.Text = quiz.getQuestions()[1].ToString();
            label3.Text = quiz.getQuestions()[2].ToString();
            label4.Text = quiz.getQuestions()[3].ToString();
        }

        private void UpdateTime() {
            label6.Text = TimeFormat(time);

            if(time <= 60) label6.ForeColor = (time > 30) ? Color.Yellow : Color.Red;
        }

        private string Percentage(bool[] arr) {
            int t = 0;
            foreach (bool i in arr)
                if (i) t++;
            return (t * 100 / arr.Length).ToString() + "%";
        }

        private string TimeFormat(int time) {
            return (time / 60).ToString() + ((time % 60 < 10) ? ":0" : ":") + (time % 60).ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (time > 0)
            {
                time--;
                UpdateTime();
            }
            else {
                timer1.Stop();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Calculator calculator = new Calculator();
            calculator.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool[] correct = { numericUpDown1.Value == quiz.getQuestions()[0].Answer, numericUpDown2.Value == quiz.getQuestions()[1].Answer, numericUpDown3.Value == quiz.getQuestions()[2].Answer, numericUpDown4.Value == quiz.getQuestions()[3].Answer };
            string message = "Correct answers: ";
            string message1 = "";
            string message2 = "";

            for(int i = 0; i < correct.Length; i++)
            {
                if(correct[i])
                {
                    message += (i + 1).ToString() + " ";
                }
                else
                {
                    if (message1 == "") message1 = "Wrong answers: ";
                    message1 += (i + 1).ToString() + " ";
                    message2 += "\nThe correct answer for question " + (i + 1).ToString() + " was " + quiz.getQuestions()[i].Answer;
                }
            }
            timer1.Stop();
            MessageBox.Show("Score: " + Percentage(correct) + "\n\nCompleted in " + (time > 0 ? TimeFormat(120 - time) : "more than 2 minutes!") + "\n\n" + message + "\n" + message1 + "\n" + message2, "Results");
            NewGame();
        }
    }
}
