﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quizculator
{
    class Question
    {
        private int n1, n2, answer;
        private char symbol;

        public int Answer { get => answer; set => answer = value; }

        public Question(int n1, int n2, char symbol) {
            this.n1 = n1;
            this.n2 = n2;
            this.symbol = symbol;

            switch (symbol) {
                case '+':
                    answer = n1 + n2;
                    break;
                case '-':
                    answer = n1 - n2;
                    break;
                case '*':
                    answer = n1 * n2;
                    break;
                case '/':
                    answer = n1 / n2;
                    break;
            }
        }

        public override string ToString() {
            return n1.ToString() + "  " + symbol + "  " + n2.ToString() + "  = ";
        }

    }
}
