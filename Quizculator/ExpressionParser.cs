﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quizculator
{
    static class ExpressionParser
    {
        public static int ParseExpression(string expression) {
            if (expression.Contains("+"))
            {
                int result = 0;
                string[] members = expression.Split('+');
                foreach (string member in members) result += ParseExpression(member);
                return result;
            }
            else if (expression.Contains("-"))
            {
                string[] members = expression.Split('-');
                int result = ParseExpression(members[0]);
                for (int i = 1; i < members.Length; i++) {
                    result -= ParseExpression(members[i]);
                }
                return result;
            }
            else if (expression.Contains("*"))
            {
                int result = 1;
                string[] members = expression.Split('*');
                foreach (string member in members) result *= ParseExpression(member);
                return result;
            }
            else if (expression.Contains("/"))
            {
                string[] members = expression.Split('/');
                int result = ParseExpression(members[0]);
                for (int i = 1; i < members.Length; i++)
                {
                    result /= ParseExpression(members[i]);
                }
                return result;
            }
            else
            {
                return int.Parse(expression);
            }
        }

    }
}
