﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quizculator
{
    public partial class Calculator : Form
    {
        public Calculator()
        {
            InitializeComponent();
        }

        private int ParseExpression(string expression)
        {
            if (expression.Contains("+"))
            {
                int result = 0;
                string[] members = expression.Split('+');
                foreach (string member in members) result += ParseExpression(member);
                return result;
            }
            else if (expression.Contains("-"))
            {
                string[] members = expression.Split('-');
                int result = ParseExpression(members[0]);
                for (int i = 1; i < members.Length; i++)
                {
                    result -= ParseExpression(members[i]);
                }
                return result;
            }
            else if (expression.Contains("*"))
            {
                int result = 1;
                string[] members = expression.Split('*');
                foreach (string member in members) result *= ParseExpression(member);
                return result;
            }
            else if (expression.Contains("/"))
            {
                string[] members = expression.Split('/');
                int result = ParseExpression(members[0]);
                for (int i = 1; i < members.Length; i++)
                {
                    result /= ParseExpression(members[i]);
                }
                return result;
            }
            else
            {
                return int.Parse(expression);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text += "1";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text += "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text += "3";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text += "4";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text += "5";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text += "6";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox1.Text += "7";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox1.Text += "8";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text += "9";
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length>=1 && !textBox1.Text.Last().Equals('+') && !textBox1.Text.Last().Equals('-') && !textBox1.Text.Last().Equals('*') && !textBox1.Text.Last().Equals('/'))
                textBox1.Text += "+";
            else if (textBox1.Text.Length == 0)
                textBox1.Text += "+";
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length >= 1 && !textBox1.Text.Last().Equals('+') && !textBox1.Text.Last().Equals('-') && !textBox1.Text.Last().Equals('*') && !textBox1.Text.Last().Equals('/'))
                textBox1.Text += "-";
            else if (textBox1.Text.Length == 0)
                textBox1.Text += "-";
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length >= 1 && !textBox1.Text.Last().Equals('+') && !textBox1.Text.Last().Equals('-') && !textBox1.Text.Last().Equals('*') && !textBox1.Text.Last().Equals('/'))
                textBox1.Text += "*";
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length >= 1 && !textBox1.Text.Last().Equals('+') && !textBox1.Text.Last().Equals('-') && !textBox1.Text.Last().Equals('*') && !textBox1.Text.Last().Equals('/'))
                textBox1.Text += "/";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            textBox1.Text += "0";
        }

        private void button17_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = ParseExpression(((textBox1.Text.StartsWith("+") || textBox1.Text.StartsWith("-") || textBox1.Text == "") ? "0" : "") + textBox1.Text + ((textBox1.Text.EndsWith("+") || textBox1.Text.EndsWith("-")) ? "0" : "")).ToString();
            }
            catch (FormatException)
            {
                label1.Text = "Invalid Expression!";
            }
            catch (DivideByZeroException) {
                label1.Text = "Division by 0 is not possible!";
            }
            catch (Exception)
            {
                label1.Text = "Error!";
            }

        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (label1.Text != "") label1.Text = "";
            textBox1.Clear();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length >= 1)
                textBox1.Text = textBox1.Text.Substring(0, textBox1.Text.Length - 1);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (label1.Text != "") label1.Text = "";
        }
    }
}
