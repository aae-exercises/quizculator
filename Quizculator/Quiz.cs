﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quizculator
{
    class Quiz
    {
        Random r;
        private int[] numbers;
        private char[] symbols;
        private Question[] questions;

        public Quiz() {
            r = new Random();
            numbers = new int[100];
            symbols = new char[] {'+', '-', '*', '/' };
            questions = new Question[4];

            for (int i = 0; i < 100; i++) {
                numbers[i] = r.Next(1, 100);
            }

            ReinitializeQuiz();
        }

        public void ReinitializeQuiz() {
            for (int i = 0; i < 4; i++)
                questions[i] = new Question(numbers[r.Next(100)], numbers[r.Next(100)], symbols[i]);
        }

        public Question[] getQuestions() {
            return questions;
        }

    }
}
